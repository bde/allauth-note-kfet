# Plugin Note Kfet 2020 pour Django Allauth

Ce module permet d'utiliser la Note Kfet en moyen d'authentification pour
Django Allauth, si vous l'utilisez comme service d'authentification pour votre
site Django.


## Installation

Vous pouvez installer le module avec `pip` :

```bash
$ pip3 install git+https://gitlab.crans.org/bde/allauth-note-kfet.git
```

La même commande permet de mettre à jour.


## Configuration

Dans le ``settings.py`` de votre site Django, ajoutez dans la liste
``INSTALLED_APPS`` après ``allauth`` le module en question :

```python
INSTALLED_APPS = (
    ...
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth_note_kfet',
    ...
)
```

Dans la configuration de Django Allauth, rajoutez :

```python
SOCIALACCOUNT_PROVIDERS = {
    ...
    'notekfet': {
      # 'DOMAIN': 'note.crans.org',  # À remplacer si nécessaire
      # 'SCOPE': ['1_1'],  # Adapter les scopes demandées (par défaut lecture des champs utilisateur⋅rice)
    },
    ...
}
```

Rendez-vous ensuite sur la Note Kfet, sur la page des applications OAuth2 :
`https://note.crans.org/o/applications/`, et créez une nouvelle application.
Appelez-là comme vous voulez, dans `client type` rentrez le type d'application
souhaitée, dans `authorization grant type` choisissez `authorization code`, et
enfin dans `Redirect uris`, rentrez
`https://monsite.example.com/accounts/notekfet/login/callback/` en adaptant
bien sûr l'URL à votre cas. Enregistrez, et conservez bien les deux
identifiants communiqués.

Enfin, sur votre propre site, rendez-vous sur Django-admin créer une nouvelle
application sociale (`socialaccount.socialapp`) de type `Note Kfet`, et rentrez
le nom souhaité (`Note Kfet` par exemple), suivis du client ID et de la clé
secrète récupérés.

Enregistrez et c'est tout :)

En ligne de commande, avec un terminal Python :

```python
from allauth.socialaccount.models import SocialApp

SocialApp.objects.create(
    provider='notekfet',
    name="Note Kfet",
    client_id="MY_CLIENT_ID",
    secret="SECRET_KEY",
)
```

Mais prenez garde à votre historique de commandes pour ne pas garder en clair
ces informations.


## Informations communiquées

Le pseudo, le prénom, le nom, l'adresse mail et l'avatar sont récupérés par ce
module.  Si un compte existe déjà avec le pseudo donné par la note, elle agit
en second moyen d'authentification et ne crée pas de nouveaux comptes, vous
devez donc avoir confiance en la Note Kfet.
