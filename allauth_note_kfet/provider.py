from allauth.account.models import EmailAddress
from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider


class NoteKfetAccount(ProviderAccount):
    def to_str(self):
        return self.account.extra_data.get("username")

    def get_avatar_url(self):
        return self.account.extra_data.get("note").get("display_image") or \
        "https://note.crans.org/static/member/img/default_picture.png"


class NoteKfetProvider(OAuth2Provider):
    id = "notekfet"
    name = "Note Kfet"
    account_class = NoteKfetAccount

    def extract_uid(self, data):
        return str(data["username"])

    def extract_common_fields(self, data):
        return dict(
            email=data.get("email"),
            username=data.get("username"),
            last_name=data.get("last_name"),
            first_name=data.get("first_name"),
        )

    def get_default_scope(self):
        return ["1_1"]

    def extract_email_addresses(self, data):
        ret = []
        email = data.get("email")
        if email:
            ret.append(EmailAddress(email=email, verified=True, primary=True))
        return ret


provider_classes = [NoteKfetProvider]
