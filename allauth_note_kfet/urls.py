from allauth_note_kfet.provider import NoteKfetProvider
from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns


urlpatterns = default_urlpatterns(NoteKfetProvider)
